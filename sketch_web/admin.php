<!DOCTYPE html>
<html lang="en">
<head>
    <title>Admin Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" type="text/css" href="style/admin.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<script>
    function adapt() {
        if ($(window).width() <= 360){
            $(".nav-btn").removeClass("fa-2x")
            $(".nav-btn").addClass("fa-1x")
        } else {
            $(".nav-btn").removeClass("fa-1x")
            $(".nav-btn").addClass("fa-2x")
        }
    }
    $(document).ready(function () {
        var cod = "none"
        adapt()
        $( window ).resize(function() {
            adapt()
        })
        $(".dropdown-item").click(function () {
            $(this).parent().parent().children(".btn").text($(this).text());
            console.log($(this).text())
            cod = $(this).attr("name")
            console.log(cod)

        })
        $("#remove-res").click(function () {
            if (cod !== "none") {
                $.post("admin_api.php", {
                    codice_ristorante: cod
                }); 
                cod = "none"
            }
        })
        $("#remove-acc").click(function () {
            if (cod !== "none") {
                $.post("admin_api.php", {
                    id: cod
                }); 
                cod = "none"              
            }
        })
    })


</script>
<?php
    include_once 'includes/db_connect.php';
    include_once 'includes/functions.php';
    sec_session_start();

    $account_query = "SELECT email, id FROM members";
    $restautrant_query = "SELECT codice_ristorante, nome FROM ristoranti";
    $acc = $mysqli->query($account_query);
    $res = $mysqli->query($restautrant_query);
    if (!login_check($mysqli)) {
        header('Location: landing.php');
        }
?>
<body>
    <header class="py-2 bg-dark">
        <div class="container-fluid">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4">
                    <a id="slide" class="text-muted" href="landing.php">
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="landing.php"><img id="logo" src="res/logo.png" alt="" width="50" height="50"></a>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                    <a id="search" class="text-muted nav-btn" href="landing.php">
                    </a>
                </div>
            </div>
        </div>
    </header>
    <section>
        <div class="container rounded">
            <section>
                <div>
                    Remove User
                </div>
                <div class="button-wrap">
                <div class="btn-group">
                    <div class="btn-group">
                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">User</button>
                        <div class="dropdown-menu">
                        <?php
                            while($row = mysqli_fetch_assoc($acc)) {
                        ?>
                            <a class="dropdown-item" href="#" name="<?php echo $row["id"]; ?>"><?php echo $row["email"]; ?></a>
                        <?php
                            }
                        ?>
                        </div>
                    </div>
                    <button id="remove-acc" type="button" class="btn btn-danger">Remove Account</button>
                </div>
                </div>
            </section>
            <section>
                <div>
                    Remove Restaurant   
                </div>
                <div class="button-wrap">
                <div class="btn-group">
                    <div class="btn-group">
                        <button type="button" class="btn btn-secondary dropdown-toggle" data-toggle="dropdown">Restaurant</button>
                        <div class="dropdown-menu">
                        <?php
                            while($row = mysqli_fetch_assoc($res)) {
                        ?>
                            <a class="dropdown-item" href="#" name="<?php echo $row["codice_ristorante"]; ?>"><?php echo $row["nome"]; ?></a>
                        <?php
                            }
                        ?>
                        </div>
                    </div>
                    <button id="remove-res" type="button" class="btn btn-danger">Remove Restaurant</button>
                </div>
                </div>
            </section>
        </div>
    </section>

</body>