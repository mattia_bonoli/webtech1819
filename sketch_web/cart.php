<!-- add different background color for disabled minus buttons and the selected 0ed item -->
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Carrello</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" type="text/css" href="style/cart.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="js/cart.js"></script>
</head>

<?php
function retrieve_price($element, $codice_ristorante, $mysqli) {
    //retrieve price from database
    $query_price = "select prezzo from piatti
                    where codice_ristorante = " . $codice_ristorante
                    . " and nome = '" . $element . "'";
    $res_price = $mysqli->query($query_price);
    if ($res_price !== false) {
        if ($price_row = $res_price->fetch_assoc()) {
            return $price_row["prezzo"];
        }
    } else {
        return "Errore di connessione";
    }
}

if (!isset($_GET["order"])) {
    echo "Errore! Nessun ordine impostato.";
} else {
    include_once 'includes/db_connect.php';
    include_once 'includes/functions.php';
    sec_session_start();
    $logged = login_check($mysqli);
    if ($logged) {
        //create associative array "order"
        $order = json_decode($_GET["order"], true);
        //check connection to database
        $codice_ristorante = $order["codice_ristorante"];
        unset($order["codice_ristorante"]);
?>

<body>
    <header class="navbar navbar-expand-sm bg-dark navbar-dark sticky-top">
        <nav class="navbar bg-dark">
            <a href="restaurant_page.php?codice_ristorante=<?php echo $codice_ristorante; ?>" class="btn btn-danger">Indietro</a>
        </nav>
    </header>
    <div class="container">
        <section id="riassunto">
            <table class="table table-striped table-responsive">
                <thead>
                    <tr>
                        <div class="headers">
                            <th id="fh" class="invisible">Quantità</th>
                            <th id="sh" class="invisible">Articolo</th>
                            <th id="th" class="invisible">Prezzo</th>
                        </div>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    //loop through the associative array an populate the table
                    foreach ($order as $element => $value) {
                    ?>
                    <div class="element">
                        <tr>
                            <td class="quantitàArticolo"><span><?php echo $value; ?></span>x</td>
                            <td class="nomeArticolo"><span><?php echo $element; ?></span></td>
                            <td class="prezzoArticolo text-right">€<span class="prezzoUn">
                            <?php
                                echo retrieve_price($element, $codice_ristorante, $mysqli);
                            ?></span></td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <button class="btn btn-outline-primary btn-sm select-r" type="button" name="plus">+</button>
                                <button class="btn btn-outline-primary btn-sm select-l" type="button" name="minus">-</button>
                            </td>
                        </tr>
                    </div>
                    <?php
                    }
                    ?>
                </tbody>
            </div>
            </table>
            <p id="totale-tabella">Totale €<span></span></p>
        </section>
        <section id="forms">
            <div class="jumbotron">
                <form class="place" action="#" method="post">
                    <fieldset>
                        <legend>Luogo consegna</legend>
                        <label><input type="radio" name="luogoCons" value="ingr1" checked> Ingresso 1, Via Abba 3</label><br/>
                        <label><input type="radio" name="luogoCons" value="ingr2"> Ingresso 2, Via Babba 4</label>
                    </fieldset>
                </form>
            </div>
            <div class="jumbotron">
                <form class="place" action="#" method="post">
                    <fieldset>
                        <legend>Ora consegna</legend>
                        <label><input type="radio" name="ora" value="asap" checked> Il prima possibile<br/></label><br/>
                        <label><input type="radio" name="ora" value="man"> Manuale</label>
                        <select id="hours" name="hours" class="custom-select">
                            <?php
                            $hours = fill_hours();
                            foreach ($hours as $h) {
                            ?>
                               <option><?php echo $h?></option>
                            <?php
                            }
                            ?>
                        </select>
                    </fieldset>
                </form>
            </div>
        </section>
        <section id="footer">
            <p id="totale">Totale €<span></span></p>
                <button class="shadow btn btn-block btn-primary btn-lg" id="pagamento" autofocus>Avanti</button>
                <a href="restaurant_page.php?codice_ristorante=<?php echo $codice_ristorante; ?>" class="shadow btn btn-block btn-danger" id="annulla">Annulla</a>
        </section>
    </div>
</body>
<script type="text/javascript">
$("document").ready(function(){
    calculateTotal();
    var x = window.matchMedia("(min-width: 576px)")
    adjustView(x) // Call listener function at run time
    x.addListener(adjustView) // Attach listener function on state changes

    //order as a modifiable javascript array
    var order = { <?php foreach ($order as $k => $v) echo $k . ':"' . $v . '", '; ?> }
    var prices = { <?php foreach ($order as $k => $v) echo $k . ':"' . retrieve_price($k, $codice_ristorante, $mysqli) . '", '; ?> }

    $("button[name='plus']").click(function(){
        var h = $(this).parent().parent().prev().children("td.quantitàArticolo").children("span").text();
        if (h == 0) {
            $(this).parent().children('button[name="minus"]').prop("disabled", false);
        }
        var hplus = Number(h) + 1;
        $(this).parent().parent().prev().children("td.quantitàArticolo").children("span").html(hplus);
        var price = $(this).parent().parent().prev().children("td.prezzoArticolo").children("span").text();
        var tot = $("#totale span").text();
        var newTot = (Number(tot) + Number(price)).toFixed(2);
        $("#totale span").html(newTot);
        $("#totale-tabella span").html(newTot);
        if (tot === "0.00") {
            $("#pagamento").prop("disabled", false);
        }
        var nomeArt = $(this).parent().parent().prev().children("td.nomeArticolo").children("span").text()
        order[nomeArt]++;
    });

    $("button[name='minus']").click(function(){
        var h = $(this).parent().parent().prev().children("td.quantitàArticolo").children("span").text();
        var hmin = Number(h) - 1;
        $(this).parent().parent().prev().children("td.quantitàArticolo").children("span").html(hmin);
        if (hmin === 0) {
            $(this).prop("disabled", true);
        }
        var price = $(this).parent().parent().prev().children("td.prezzoArticolo").children("span").text();
        var tot = $("#totale span").text();
        var newTot = (Number(tot) - Number(price)).toFixed(2);
        $("#totale span").html(newTot);
        $("#totale-tabella span").html(newTot);
        if (newTot === "0.00") {
            $("#pagamento").prop("disabled", true);
        }
        var nomeArt = $(this).parent().parent().prev().children("td.nomeArticolo").children("span").text()
        order[nomeArt]--;
    })

    $("#hours").hide();

    $("input[value='man']").click(function(){
        $("#hours").show();
    })

    $("input[value='asap']").click(function(){
        $("#hours").hide();
    })

    $("#pagamento").click(function(){
        $(this).prop("disabled", true);
        $(this).html('Attendere...');

        //get useful data
        var prezzoTot = $("#totale span").text();
        var timeVal = $("input[name='ora']:checked").val();
        var acTime = timeVal==='asap' ? $('option:first').text() : $('select').val();
        var place = $("input[name='luogoCons']:checked").val();
        //ingr1 = via abba 3, ingr 2 = via babba 4

        $.post('send_notification.php',
        {
            order: JSON.stringify(order),
            prezziUn: JSON.stringify(prices),
            totale: prezzoTot,  //redundant
            oraProposta: acTime,
            luogoCons: place,
            codice_rist: <?php echo $codice_ristorante; ?>
        },
        function(response) {
            alert(response);
            location.href = 'main_page.php';
        });
    })
})
</script>
<?php
    $mysqli->close();
    } else {
        echo "Non sei loggato! Prima di effettuare un ordine devi fare login!";
    }
}
?>
</html>
