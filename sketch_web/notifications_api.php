<?php
include_once "includes/notification_functions.php";

if(isset($_POST["request"])){
    switch ($_POST["request"]) {
        case "confirm":
            //invia conferma a utente (anche per mail via api ajax) con nuovo orario
            //imposta letta = s su database
            //inserisci il nuovo orario su database nella tabella prenotazioni
            if (isset($_POST['sub']) && isset($_POST['body']) && isset($_POST['name'])
                && isset($_POST["codice_pren"]) && isset($_POST['nuova_ora'])
                && isset($_POST["cod_not_rist"]) && isset($_POST["id_utente"])) {
                send_email($_POST['sub'], $_POST['body'], $_POST['name'], get_email($_POST["id_utente"]));
                $qry = "INSERT INTO notifica_per_utente (codice_prenotazione, codice_notifica_ristorante, descrizione, letta, id) VALUES (" . $_POST['codice_pren'] . "," . $_POST['cod_not_rist'] . ",'" . $_POST['body'] . "','n'," . $_POST['id_utente'] . ")";
                $qry1 = "UPDATE notifica_per_ristorante SET letta = 's' WHERE codice_prenotazione = " . $_POST["codice_pren"];
                $qry2 = "UPDATE prenotazioni SET ora_prenotazione = '" . $_POST['nuova_ora'] . "' WHERE codice_prenotazione = " . $_POST["codice_pren"];
                $ris = $mysqli->query($qry);
                if ($ris !== false) {
                    $ris = $mysqli->query($qry1);
                    if ($ris !== false) {
                        $ris = $mysqli->query($qry2);
                        if ($ris === false) {
                            echo "Errore nella query di update orario";
                        }
                    } else {
                        echo "Errore nella query di update lettura";
                    }
                } else {
                    echo "Errore nella query di inserimento notifica utente";
                }
            } else {
                echo "Errore nel passaggio dei parametri post!";
            }
            break;
        case "delete":
            if (isset($_POST["cod_not_rist"]) && isset($_POST['type'])) {
                if ($_POST['type'] === 'business') {
                    $qry = "DELETE FROM notifica_per_ristorante WHERE codice_notifica_ristorante =" . $_POST["cod_not_rist"];
                } else {
                    $qry = "DELETE FROM notifica_per_utente WHERE codice_notifica_ristorante =" . $_POST["cod_not_rist"];
                }
                $ris = $mysqli->query($qry);
                if ($ris === false) {
                    echo "Errore nella query di eliminazione";
                }
            }
            break;
        default:
            break;
    }
}
 ?>
