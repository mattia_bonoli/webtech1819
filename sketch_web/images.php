<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <title>Aggiunta immagini</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
        <link rel="stylesheet" type="text/css" href="style/modal.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="style/images.css">
    </head>
    <?php
        include_once 'includes/db_connect.php';
        include_once 'includes/functions.php';
        sec_session_start();
    ?>
    <script>
        $(document).ready(function () {
            var userid = <?php echo $_SESSION["user_id"]; ?>;

            $("#copertina").click(function () {
                $.get("images_api.php", {
                    type: "copertina",
                    id: userid,
                    nome: $(".copertina").val()
                });
                console.log($(".copertina").val())
            })
            $("#carousel").click(function () {
                $.get("images_api.php", {
                    type: "carousel",
                    id: userid,
                    nome: $(".carousel").val()
                });
            })
        })
    </script>
    <body>
    <header class="py-2 bg-dark">
        <div class="container-fluid">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4">
                    <a id="slide" class="text-muted" href="#">
                        <i class="fas fas fa-angle-right fa-2x" data-toggle="modal" data-target="#sideModal"></i>
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="main_page.php"><img id="logo" src="res/logo.png" alt="Logo" width="50" height="50"></a>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                </div>
            </div>
        </div>
    </header>
    <div class="modal left fade" id="sideModal" tabindex="-1" role="dialog" aria-labelledby="sideModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="list-group list-group-flush">
                        <?php
                        $logged = login_check($mysqli);
                        if (login_check($mysqli) == true) {
                            echo '<h4 class="py-3">Benvenuto, '. htmlentities($_SESSION['username']) . '</h4>';
                            echo '<a href="notifiche.php" class="btn btn-light btn-lg btn-block m-1">Notifiche</a>';
                            echo '<a href="modify_menu.php" class="btn btn-light btn-lg btn-block m-1">Modifica menù</a>';
                            echo '<a href="user.php" class="btn btn-light btn-lg btn-block m-1">Gestisci Account</a>';
                            echo '<a href="includes/logout.php" class="btn btn-light btn-lg btn-block m-1">Log out</a>';
                        }else {
                            echo '<h4 class="pb-3">Per accedere a queste pagine, effettua il <a href="login.php">login</a>.</h4>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Notifiche</a>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Gestisci Account</a>';

                        }
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
        <div class="container">
            <form class="form-group" id="foodform" action="insertfood_api.php" method="post">
                <div class="form-group">
                    <label>Nome immagine copertina: <input type="text" class="form-control copertina" placeholder="Nome immmagine" name="nome" autofocus></label><br>
                    <button id="copertina" type="button" class="btn btn-primary" name="button">Cambia</button>
                </div>
                <div class="form-group">
                    <label>Nome immagine carousel: <input type="text" class="form-control carousel" placeholder="Nome immmagine" name="nome" autofocus></label><br>
                    <button id="carousel" type="button" class="btn btn-primary" name="button">Aggiungi</button>
                </div>
            </form>
        </div>
    </body>
</html>
