<!DOCTYPE html>
<html>
<head>
    <title>Landing Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" type="text/css" href="style/landing.css">
    <link rel="stylesheet" type="text/css" href="style/footer.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<script>

    function adapt() {
        if ($(window).width() <= 360){
            $(".sfoglia").removeClass("btn-lg")
            $(".sfoglia").addClass("btn-sm")
            $("#logo").hide()

        } else {
            $(".sfoglia").removeClass("btn-sm")
            $(".sfoglia").addClass("btn-lg")
            $("#logo").show()
        }
    }

    $(document).ready(function () {
        adapt();

        $( window ).resize(function() {
            adapt();
        })
    })

</script>
<?php
    include_once 'includes/db_connect.php';
    include_once 'includes/functions.php';
    sec_session_start();

    $logged = login_check($mysqli);
?>
<body>

    <div class="hero-image">
        <header class="py-2 bg-dark">
            <div class="container-fluid">
                <div class="row flex-nowrap justify-content-between align-items-center">
                    <div class="col-4">
                    </div>
                    <div id="logo" class="col-4 text-center">
                        <a href="landing.php"><img src="res/logo.png" alt="" width="50" height="50"></a>
                    </div>
                    <div class="col-4 d-flex justify-content-end align-items-center">
                        <?php
                            if(!$logged) {
                         ?>
                            <a href="login.php"><button class="btn btn-success login account" type="submit" style="margin-right: 20px;">Login</button></a>
                            <a href="register.php"><button class="btn btn-secondary account" type="submit">Sign up</button></a>
                        <?php
                        }
                         ?>
                    </div>
                </div>
            </div>
        </header>


        <div id="text" class="container">
            <h1>Benvenuto!</h1>
            <p>Sfoglia il nostro catalogo</p>
        <!--<div class="input-group mb-3">
                <input type="text" class="form-control" placeholder="Cerca ristoranti...">
                <div class="input-group-append">
                    <button class="btn btn-success" type="submit">Go</button>
                </div>
            </div>-->
            <a href="main_page.php">
                <?php
                    if(!$logged){
                        ?>
                        <button class="btn btn-primary sfoglia" type="submit">Catalogo ristoranti</button>
                        <?php
                    } else if($_SESSION["userType"] == "user"){
                        ?>
                        <button class="btn btn-primary sfoglia" type="submit">Catalogo ristoranti</button>
                        <?php
                    } else {
                        ?>
                        <button class="btn btn-primary sfoglia" type="submit">Accedi account</button>
                        <?php
                    }
                ?>
            </a>
        </div>
    </div>

    <footer>
        <div class="footer-copyright text-center py-2 bg-dark fixed-bottom" style="color: white;">
            Tecnologie web 2018/2019 <br>
            Credits: Filippo Pistocchi, Mattia Bonoli, Federico Cichetti.
        </div>
    </footer>
</body>
</html>
