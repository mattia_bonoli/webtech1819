<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <title>Modifica menù</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
        <link rel="stylesheet" type="text/css" href="style/modal.css">
        <link rel="stylesheet" type="text/css" href="style/modify_menu.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    </head>
    <body>
        <?php
        include_once 'includes/db_connect.php';
        include_once 'includes/functions.php';
        sec_session_start();
        $logged = login_check($mysqli);
        if($logged && $_SESSION["userType"] === "business") {
         ?>
             <header class="py-2 bg-dark">
        <div class="container-fluid">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4">
                    <a id="slide" class="text-muted" href="#">
                        <i class="fas fas fa-angle-right fa-2x" data-toggle="modal" data-target="#sideModal"></i>
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="main_page.php"><img id="logo" src="res/logo.png" alt="Logo" width="50" height="50"></a>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                </div>
            </div>
        </div>
    </header>
    <div class="modal left fade" id="sideModal" tabindex="-1" role="dialog" aria-labelledby="sideModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="list-group list-group-flush">
                        <?php
                        if (login_check($mysqli) == true) {
                            echo '<h4 class="py-3">Benvenuto, '. htmlentities($_SESSION['username']) . '</h4>';
                            echo '<a href="user.php" class="btn btn-light btn-lg btn-block m-1">Gestisci Account</a>';
                            if($_SESSION["userType"] === "user") {
                                echo '<a href="orders.php" class="btn btn-light btn-lg btn-block m-1">I miei ordini</a>';
                            }
                            if($_SESSION["userType"] == "business") {
                                echo '<a href="notifiche.php" class="btn btn-light btn-lg btn-block m-1">Notifiche</a>';
                                echo '<a href="images.php" class="btn btn-light btn-lg btn-block m-1">Gestisci Immagini</a>';
                            }
                            echo '<a href="includes/logout.php" class="btn btn-light btn-lg btn-block m-1">Log out</a>';
                        }else {
                            echo '<h4 class="pb-3">Per accedere a queste pagine, effettua il <a href="login.php">login</a>.</h4>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Gestisci Account</a>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">I miei ordini</a>';

                        }
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
        <div class="container">
            <form class="form-group" id="foodform" action="insertfood_api.php" method="post">
                <div class="form-group" id="first-row">
                    <label>Nome piatto: <input type="text" class="form-control" placeholder="Inserire nome piatto" id="nome" autofocus></label>
                    <label>Categoria: <input type="text" class="form-control" placeholder="Inserire categoria" id="categoria"></label>
                    <label for="prezzo">Prezzo: <br/>
                    <input id="prezzo" type="number" name="prezzo" min="1" step="any"></label>
                </div>
                <div class="form-group">
                    <label for="desc">Descrizione: <br/>
                    <textarea id="desc" name="descrizione" placeholder="Inserisci descrizione del piatto" rows="4" cols="80" form="foodform"></textarea></label>
                </div>
                <div class="form-group">
                    <label>Allergeni: <input type="text" id="allergeni"></label>
                </div>
                <input type="hidden" id="codice_res" value="
                <?php
                    $qry = "SELECT codice_ristorante FROM ristoranti WHERE id=" . $_SESSION["user_id"];
                    $ris = $mysqli->query($qry);
                    if ($row = $ris->fetch_assoc()){
                        echo $row["codice_ristorante"];
                    }
                ?>">
                <button type="button" class="btn btn-primary" name="button">Aggiungi Piatto</button>
            </form>
        </div>
    </body>
    <script type="text/javascript">
        $(document).ready(function(){
            $("button").click(function(){
                $.post("insertfood_api.php", {
                    nome: $("#nome").val(),
                    categoria: $("#categoria").val(),
                    prezzo: $("#prezzo").val(),
                    descrizione: $("#desc").val(),
                    allergeni: $("#allergeni").val(),
                    codice_res: $("#codice_res").val()
                })
                $('#foodform').trigger("reset");
            })
        })
    </script>
<?php } else {
    echo "Questa è una pagina per soli account business...";
} ?>
</html>
