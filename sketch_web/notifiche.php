<!DOCTYPE html>
<html lang="it">
<head>
    <title>Notifiche</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="style/main_page_style.css">
    <link rel="stylesheet" type="text/css" href="style/modal.css">
    <link rel="stylesheet" href="style/notifiche.css">
    <link rel="stylesheet" href="style/footer.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<?php
    include_once 'includes/notification_functions.php';
    sec_session_start();
    $logged = login_check($mysqli);
    if($logged) {
        $qry = "SELECT type FROM members WHERE id=" . $_SESSION['user_id'];
        $ris = $mysqli->query($qry);
        if($ris->num_rows>0) {
            $row = $ris->fetch_assoc();
            $account_type = $row['type'];
        }
?>
<body>
    <header class="py-2 bg-dark">
        <div class="container-fluid">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4">
                    <a id="slide" class="text-muted" href="#">
                        <i class="fas fas fa-angle-right fa-2x" data-toggle="modal" data-target="#sideModal"></i>
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="main_page.php"><img id="logo" src="res/logo.png" alt="Logo" width="30" height="30"></a>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                </div>
            </div>
        </div>
    </header>
    <div class="modal left fade" id="sideModal" tabindex="-1" role="dialog" aria-labelledby="sideModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="list-group list-group-flush">
                        <?php
                        if (login_check($mysqli) == true) {
                            echo '<h4 class="py-3">Benvenuto, '. htmlentities($_SESSION['username']) . '</h4>';
                            echo '<a href="user.php" class="btn btn-light btn-lg btn-block m-1">Gestisci Account</a>';
                            if($_SESSION["userType"] === "user") {
                                echo '<a href="orders.php" class="btn btn-light btn-lg btn-block m-1">I miei ordini</a>';
                            }
                            if($_SESSION["userType"] == "business") {
                                echo '<a href="modify_menu.php" class="btn btn-light btn-lg btn-block m-1">Modifica Menù</a>';
                                echo '<a href="images.php" class="btn btn-light btn-lg btn-block m-1">Gestisci Immagini</a>';
                            }
                            echo '<a href="includes/logout.php" class="btn btn-light btn-lg btn-block m-1">Log out</a>';
                        }else {
                            echo '<h4 class="pb-3">Per accedere a queste pagine, effettua il <a href="login.php">login</a>.</h4>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Gestisci Account</a>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">I miei ordini</a>';

                        }
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <!--Trasporre questo stile nel .css -->
    <p id="no-not">Non c'è nulla da mostrare...</p>
    <section>
        <div class="container">
            <!-- This will be filled with notifications -->
        </div>
    </section>
    <footer>
        <div class="footer-copyright text-center py-2 bg-dark" style="color: white;">
            Tecnologie web 2018/2019 <br>
            Credits: Filippo Pistocchi, Mattia Bonoli, Federico Cichetti.
        </div>
    </footer>
</body>
<script type="text/javascript">
    function loadData(){
        $(".container").load("notification_data.php?type=" + "<?php echo $account_type; ?>" + " #notifiche", reloadControls);
    }

    function reloadControls(){
        $("#no-not").hide();
        var account_type = '<?php echo $account_type; ?>'
        num_not = 0;
        $(".notifica").each(function(index, this_not){
            num_not++;
            var codice_pren = $(this_not).children("input[name='cod']").val();
            var codice_rist = $(this_not).children("input[name='cod_rist']").val();
            if (account_type === 'business' && $(this).children("input[name='seen']").val() === 'n') {
                $(this).children(".delete").hide();
                $(this).children(".conferma").click(function(){
                    //prendi nuovo orario
                    var new_hour = $(this_not).children(".hours").children("select").val();
                    var id = $(this_not).children("input[name='id_ut']").val();
                    //rimuovi i bottoni di controllo ordine
                    $(this_not).children(".conferma").remove();
                    $(this_not).children(".hours").remove();
                    //attiva il bottone delete per cancellare la notifica
                    $(this_not).children(".delete").show();

                    //cambia orario visibile
                    $(this_not).children("h1").html("Orario richiesto: " + new_hour);

                    //invia conferma a utente (anche per mail via api ajax) con nuovo orario
                    //imposta letta = s su database
                    //inserisci il nuovo orario su database nella trabella prenotazioni
                    $.post("notifications_api.php", {
                        request: "confirm",
                        sub: "Ordine confermato dal ristorante!",
                        body: "Il vostro ordine è stato confermato. Sarà consegnato alle ore " + new_hour,
                        name: '<?php echo htmlentities($_SESSION['username']); ?>',
                        codice_pren: codice_pren,
                        nuova_ora: new_hour,
                        cod_not_rist: codice_rist,
                        id_utente: id
                    })
                })
            } else {
                $(this_not).children(".conferma").remove();
                $(this_not).children(".hours").remove();
            }
            //la funzione a delete va applicata in ogni caso perchè il bottone si attiverà quando l'ordine viene confermato
            $(this_not).children(".delete").click(function(){
                //rimuovo notifica (con transizione magari)
                $(this_not).remove();
                num_not--;
                if (num_not === 0) {
                    $("#no-not").show();
                }

                //rimuovo la notifica su database
                $.post("notifications_api.php", {
                    request: "delete",
                    type: account_type,
                    cod_not_rist: codice_rist
                })
            })
        })
        //si mantiene questo controllo perchè è possibile che semplicemente non ci siano notifiche
        if (num_not === 0) {
            $("#no-not").show();
        }
    }

    $(document).ready(function(){
        //constantly query the database
        setInterval(loadData, 30000); //once every thirty seconds
        //and also query it for the first time
        loadData();
    })
</script>
<?php
$mysqli->close();
} else {
    echo "Per visualizzare le proprie notifiche è necessario effettuare il login.";
}
?>
</html>
