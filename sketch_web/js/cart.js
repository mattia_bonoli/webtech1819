function adjustView(x) {
    if (x.matches) { // If media query matches
        //hiding the table head currently doesn't work for whatever reason
        //$("#riassunto table th").hide();
        $("#riassunto p").hide();
    } else {
        //$("#riassunto table th").show();
        $("#riassunto p").show();
    }
}

function calculateTotal() {
    var tot = 0.00;
    $("table").children("tbody").children("tr").each(function() {
        var quant = $(this).children("td.quantitàArticolo").children("span").text();
        var pr = $(this).children("td.prezzoArticolo").children("span").text();
        tot += Number(quant) * Number(pr);
    });
    $("#totale span").html(tot.toFixed(2));
    $("#totale-tabella span").html(tot.toFixed(2));
}
