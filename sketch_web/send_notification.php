<?php

include_once 'includes/notification_functions.php';
sec_session_start();
$logged = login_check($mysqli);

function printTable($order, $prices) {
    $table = '';
    foreach ($order as $n => $q) {
        if ($q > 0) {
            $table .= "<tr><td>" . $q . "</td><td>" . $n . "</td><td>" . $prices->$n . "€</td></tr>";
        }
    }
    return $table;
}

function send_mail($order, $desc) {
    $sub = 'Il tuo ordine presso UniFood';
    $body    = "
    <h1>Ciao, <strong>" . htmlentities($_SESSION['username']) . "</strong></h1>
    <p>Il tuo ordine è stato ricevuto! Eccoti un piccolo riassunto:</p>" . $desc;
    send_email($sub, $body, htmlentities($_SESSION['username']), get_email($_SESSION['user_id']));
}

function queryID($foodname) {
    $selectqry = "SELECT codice_piatto FROM piatti WHERE codice_ristorante=" . $GLOBALS['cod_ris'] . " AND nome='" . $foodname . "';";
    $result = $GLOBALS['mysqli']->query($selectqry);
    if($row = $result->fetch_assoc()) {
        return $row['codice_piatto'];
    }
}

function getIDs($order) {
    $order_ids = array_keys($order);
    return array_map("queryID", $order_ids);
}

function executeQ($qry) {
    if ($GLOBALS['mysqli']->query($qry) !== TRUE) {
        echo "Error: " . $qry . "<br>" . $GLOBALS['mysqli']->error;
        return false;
    } else {
        return true;
    }
}

function execute_query($ora, $luogo, $order, $order_ids, $desc) {
    $ok = 0;
    $i = 0;
    $qry = "INSERT INTO prenotazioni (data_prenotazione, ora_prenotazione, luogo_consegna, id)
    VALUES ('" . date('Y-m-d') . "', '" . $ora . "', '" . $luogo . "', " . $_SESSION['user_id'] . ")";
    if (executeQ($qry)) $ok++;
    $qry = "SET @id_prenotazione = LAST_INSERT_ID()";
    if (executeQ($qry)) $ok++;
    foreach ($order as $n => $q) {
        if ($q > 0) {
            $qry = "INSERT INTO piatti_in_prenotazione (numero_piatti, codice_piatto, codice_prenotazione)
            VALUES (" . $q . ", " . $order_ids[$i] . ", @id_prenotazione)";
            if (executeQ($qry)) $ok++;
        }
        $i++;
    }
    $qry = 'INSERT INTO notifica_per_ristorante (codice_prenotazione, descrizione, letta, codice_ristorante)
            VALUES (@id_prenotazione, "' . $desc . '", "n", ' . $GLOBALS["cod_ris"] . ')';
    if (executeQ($qry)) $ok++;
    return $ok;
}

if ( $_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['order'])
                                            && isset($_POST['totale'])
                                            && isset($_POST['prezziUn'])
                                            && isset($_POST['oraProposta'])
                                            && isset($_POST['luogoCons'])
                                            && isset($_POST['codice_rist'])
                                            && $logged)
{
    //order is a json array
    $order = json_decode($_POST['order']);
    $prices = json_decode($_POST['prezziUn']);
    $cod_ris = intval($_POST['codice_rist']);
    $desc =
    "<table>
        <tr>
            <th>Quantità</th>
            <th>Articolo</th>
            <th>Prezzo Unitario</th>
        </tr>" . printTable($order, $prices) . "
    </table>
    <p><strong>Totale: " . $_POST["totale"] . "€</strong></p>";
    $ora = $_POST['oraProposta'];
    $luogo = $_POST['luogoCons'];

    /* send mail */
    send_mail($order, $desc);
    /* get food id */
    $order_ids = getIDs((array)$order);
    /* populate database */
    $ok = execute_query($ora, $luogo, $order, $order_ids, $desc);
    if ($ok >= 4) {
        echo "Ordine completato con successo!";
    }

    // TODO: this session variable is basically pointless since main page will check on database for new notifications depending on user
    $_SESSION['show_notif_ok'] = true;
} else {
    header("Location: main_page.php");
    die();
}
?>
