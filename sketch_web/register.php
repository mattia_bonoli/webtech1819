<?php
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';
include_once 'includes/db_connect.php';
sec_session_start();
if (login_check($mysqli) == true) {
            header('Location: landing.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Iscrizione</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" type="text/css" href="style/register.css">
    <link rel="stylesheet" type="text/css" href="style/modal.css">
    <link rel="stylesheet" type="text/css" href="style/footer.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script type="text/JavaScript" src="js/sha512.js"></script>
    <script type="text/JavaScript" src="js/forms.js"></script>
</head>
<body>
    <!-- Registration form to be output if the POST variables are not
    set or if the registration script caused an error. -->
<script>
    $(document).ready(function f() {
        //Radio defaults on normal user
        $("#business_name").parent().hide();
        $("#p_iva").parent().hide();

        $("#typeUser").click(function f() {
            $("#business_name").parent().hide();
            $("#p_iva").parent().hide();
            $("#name").parent().show();
            $("#surname").parent().show();
        });
        $("#typebusiness").click(function f() {
            $("#name").parent().hide();
            $("#surname").parent().hide();
            $("#business_name").parent().show();
            $("#p_iva").parent().show();
        });
    })
</script>
<!--SIDE MODAL-->
<div class="modal left fade" id="sideModal" tabindex="-1" role="dialog" aria-labelledby="sideModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="list-group list-group-flush">
                    <?php
                    if (login_check($mysqli) == true) {
                        echo '<h4 class="py-3">Benvenuto, '. htmlentities($_SESSION['username']) . '</h4>';
                        echo '<a href="#" class="btn btn-light btn-lg btn-block m-1">Notifiche</a>';
                        echo '<a href="#" class="btn btn-light btn-lg btn-block m-1">Gestisci Account</a>';
                        echo '<a href="#" class="btn btn-light btn-lg btn-block m-1">I miei Ordini</a>';
                    }else {
                        echo '<h4 class="pb-3">Per accedere a queste pagine, effettua il <a href="login.php">login</a>.</h4>';
                        echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Notifiche</a>';
                        echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Gestisci Account</a>';
                        echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">I miei Ordini</a>';
                    }
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<header class="py-2 bg-dark">
    <div class="container-fluid">
        <div class="row flex-nowrap justify-content-between align-items-center">
            <div class="col-4">
                <a id="slide" class="text-muted" href="#">
                    <i class="fas fas fa-angle-right fa-2x" data-toggle="modal" data-target="#sideModal"></i>
                </a>
            </div>
            <div class="col-4 text-center">
                <a href="main_page.php"><img id="logo" src="res/logo.png" alt="Logo" width="45" height="45"></a>
            </div>
            <div class="col-4 d-flex justify-content-end align-items-center">
            </div>
        </div>
    </div>
</header>
<div class="container py-3" >
        <h2>Crea il tuo account</h2>
        <?php
        if (!empty($error_msg)) {
            echo $error_msg;
        }
        ?>
        <p>
        <ul>
            <li>L'username può contenere solo lettere maiuscole e minuscole, numeri e underscore</li>
            <li>La password deve essere di almeno 6 caratteri e contenere
                <ul>
                    <li>Almeno una lettera maiuscola (A..Z)</li>
                    <li>Almeno una lettera minuscola (a..z)</li>
                    <li>Almeno un numero (0..9)</li>
                </ul>
            </li>
        </ul>
        </p>
        <form action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>" method="post" name="registration_form">
            <label for="form-check-inline">
                Che tipologia di utente sei?<br>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" id="typeUser" name="userType" value="user" checked>Privato
                    </label>
                </div>
                <div class="form-check-inline">
                    <label class="form-check-label">
                        <input type="radio" class="form-check-input" id="typebusiness" name="userType" value="business">Azienda
                    </label>
                </div>
            </label>
            <div class="form-group">
                <label for="business_name">Ragione Sociale:</label>
                <input type="text" class="form-control" id="business_name" name="business_name">
            </div>
            <div class="form-group">
                <label for="p_iva">Partita Iva:</label>
                <input type="number" class="form-control" id="p_iva" name="p_iva">
            </div>

            <div class="form-group">
                <label for="usr">Nome:</label>
                <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group">
                <label for="pwd">Cognome:</label>
                <input type="text" class="form-control" id="surname" name="surname">
            </div>

            <div class="form-group">
                <label for="phone">Cellulare:</label>
                <input type="number" class="form-control" id="phone" name="phone" >
            </div>
            <div class="form-group">
                <label for="usr">Username:</label>
                <input type="text" class="form-control" id="usr" name="username">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" class="form-control" id="pwd" name="password">
            </div>
            <div class="form-group">
                <label for="confirmpwd">Conferma password:</label>
                <input type="password" class="form-control" id="confirmpwd" name="confirmpwd">
            </div>
            <button type="submit" class="btn btn-primary" onclick="return regformhash(this.form,
                            this.form.username,
                            this.form.email,
                            this.form.password,
                            this.form.confirmpwd);">Registrati
            </button>
        </form>
        <p>Hai già un account? Effettua il <a href="login.php">login</a>.</p>
</div>
<footer>
    <div class="footer-copyright text-center py-0 bg-dark" style="color: white;">
        Tecnologie Web 2018/2019 <br>
        Credits: Filippo Pistocchi, Mattia Bonoli, Federico Cichetti.
    </div>
</footer>
</body>
</html>
