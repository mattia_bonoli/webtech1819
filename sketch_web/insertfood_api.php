<?php
    include_once 'includes/db_connect.php';
    include_once 'includes/functions.php';
    sec_session_start();

    $query = "INSERT INTO `piatti`(`codice_piatto`, `nome`, `categoria`, `prezzo`, `descrizione`, `allergeni`, `codice_ristorante`) 
              VALUES ('',?,?,?,?,?,?)";

    $stmt = $mysqli->prepare($query);
    $stmt->bind_param("sssssi", $nome, $categoria, $prezzo, $descrizione, $allergeni, $codice_res);

    $nome = $_POST["nome"];
    $categoria = $_POST["categoria"];
    $prezzo = $_POST["prezzo"];
    $descrizione = $_POST["descrizione"];
    $allergeni = $_POST["allergeni"];
    $codice_res = $_POST["codice_res"];

    $stmt->execute();
?>