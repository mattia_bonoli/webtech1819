<?php
include_once 'psl-config.php';

function sec_session_start()
{
    $session_name = 'sec_session_id';   // Set a custom session name
    $secure = SECURE;
    // This stops JavaScript being able to access the session id.
    $httponly = true;
    // Forces sessions to only use cookies.
    if (ini_set('session.use_only_cookies', 1) === false) {
        header("Location: ../error.php?err=Could not initiate a safe session (ini_set)");
        exit();
    }
    // Gets current cookies params.
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly);
    // Sets the session name to the one set above.
    session_name($session_name);
    session_start();            // Start the PHP session
    session_regenerate_id();    // regenerated the session, delete the old one.
}

function login($email, $password, $mysqli)
{
// Using prepared statements means that SQL injection is not possible.
if ($stmt = $mysqli->prepare("SELECT id, username, password, type
    FROM members
   WHERE email = ?
    LIMIT 1")) {
    $stmt->bind_param('s', $email);  // Bind "$email" to parameter.
    $stmt->execute();    // Execute the prepared query.
    $stmt->store_result();

    // get variables from result.
    $stmt->bind_result($user_id, $username, $db_password, $type);
    $stmt->fetch();

    if ($stmt->num_rows == 1) {
            if (password_verify($password, $db_password)) {
                // Password is correct!
                // Get the user-agent string of the user.
                $user_browser = $_SERVER['HTTP_USER_AGENT'];
                // XSS protection as we might print this value
                $user_id = preg_replace("/[^0-9]+/", "", $user_id);
                $_SESSION['user_id'] = $user_id;
                // XSS protection as we might print this value
                $username = preg_replace("/[^a-zA-Z0-9_\-]+/", "", $username);
                $_SESSION['username'] = $username;
                $_SESSION['login_string'] = hash(
                    'sha512',
                          $db_password . $user_browser
                );
                $_SESSION['userType'] = $type;
                // Login successful.
                return true;
            } else {
                return false;
            }
        }
    } else {
        // No user exists.
        return false;
    }
}


function login_check($mysqli)
{
    // Check if all session variables are set
    if (isset($_SESSION['user_id'],
                        $_SESSION['username'],
                        $_SESSION['login_string'])) {
        $user_id = $_SESSION['user_id'];
        $login_string = $_SESSION['login_string'];
        $username = $_SESSION['username'];

        // Get the user-agent string of the user.
        $user_browser = $_SERVER['HTTP_USER_AGENT'];

        if ($stmt = $mysqli->prepare("SELECT password
                                      FROM members
                                      WHERE id = ? LIMIT 1")) {
            // Bind "$user_id" to parameter.
            $stmt->bind_param('i', $user_id);
            $stmt->execute();   // Execute the prepared query.
            $stmt->store_result();

            if ($stmt->num_rows == 1) {
                // If the user exists get variables from result.
                $stmt->bind_result($password);
                $stmt->fetch();
                $login_check = hash('sha512', $password . $user_browser);

                if (hash_equals($login_check, $login_string) ){
                    // Logged In!!!!
                    return true;
                } else {
                    // Not logged in
                    return false;
                }
            } else {
                // Not logged in
                return false;
            }
        } else {
            // Not logged in
            return false;
        }
    } else {
        // Not logged in
        return false;
    }
}

function esc_url($url) {

    if ('' == $url) {
        return $url;
    }

    $url = preg_replace('|[^a-z0-9-~+_.?#=!&;,/:%@$\|*\'()\\x80-\\xff]|i', '', $url);

    $strip = array('%0d', '%0a', '%0D', '%0A');
    $url = (string) $url;

    $count = 1;
    while ($count) {
        $url = str_replace($strip, '', $url, $count);
    }

    $url = str_replace(';//', '://', $url);

    $url = htmlentities($url);

    $url = str_replace('&amp;', '&#038;', $url);
    $url = str_replace("'", '&#039;', $url);

    if ($url[0] !== '/') {
        // We're only interested in relative links from $_SERVER['PHP_SELF']
        return '';
    } else {
        return $url;
    }
}

function fill_hours() {
    $hours = array();
    //used in cart.php and notifiche.php
    //set quarters and max hour
    $quarters = array("00", 15, 30, 45);
    $closing_hour = 18;
    $opening_hour = 9;

    $current_hour = date('H');
    $current_min = date('i');
    //check next quarter
    if ($current_min < $quarters[1]) $next_quarter = 1;
    else if ($current_min < $quarters[2]) $next_quarter = 2;
    else if ($current_min < $quarters[3]) $next_quarter = 3;
    else { $next_quarter = 0; $current_hour++; }
    $first = true;
    //if current hour is lower than 10 am, set it to 9 am (opening hour)
    if ($current_hour < $opening_hour) {
        $current_hour = $opening_hour;
        $next_quarter = 0;
    }
    //loop the options
    for ($h = $current_hour; $h < $closing_hour; $h++) {
        $q = ($first ? $next_quarter : 0);
        for (; $q < 4; $q++) {
            array_push($hours, $h . ":" . $quarters[$q]);
        }
        if ($first) $first = false;
    }
    return $hours;
}
