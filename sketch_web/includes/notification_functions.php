<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

function send_email($subject, $body, $name, $address) {
    include_once 'includes/mail_prepare.php';
    $mail = prepare_mail();
    //Recipients
    $mail->setFrom('no-reply@project.com', 'UniFood');
    $mail->addAddress($address, $name);  // Add a recipient

    //Content
    $mail->isHTML(true);
    $mail->Subject = $subject;
    $mail->Body    = $body;

    $mail->send();
}

function get_email($id) {
    $mailqry = "SELECT email FROM members WHERE id=". $id;
    $ris = $GLOBALS['mysqli']->query($mailqry);
    $row = $ris->fetch_assoc();
    return $row['email'];
}
?>
