<?php
include_once 'db_connect.php';
include_once 'includes/functions.php';
include_once 'psl-config.php';
$error_msg = "";
// op is old pwd hased p is new pwd hash
//check that user is logged in before changing anything
if (isset($_POST['username'],$_POST['phone'],$_POST['email'],$_POST['p'], $_POST['op'])) {
    $username = filter_input(INPUT_POST, 'username', FILTER_SANITIZE_STRING);
    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);
    $email = filter_var($email, FILTER_VALIDATE_EMAIL);
    $type = filter_input(INPUT_POST, 'userType', FILTER_SANITIZE_STRING);
    $phone = filter_input(INPUT_POST, 'phone', FILTER_SANITIZE_STRING);
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        // Not a valid email
        $error_msg .= '<p class="error">L\'email inserita non è valida.</p>';
    }

    $oldPassword = filter_input(INPUT_POST, 'op', FILTER_SANITIZE_STRING);
    if (strlen($oldPassword) != 128) {
        // The hashed pwd should be 128 characters long.
        // If it's not, something really odd has happened
        $error_msg .= '<p class="error">Configurazione password errata.</p>';
    }
    $newPassword = filter_input(INPUT_POST, 'p', FILTER_SANITIZE_STRING);
    if (strlen($newPassword) != 128) {
        // The hashed pwd should be 128 characters long.
        // If it's not, something really odd has happened
        $error_msg .= '<p class="error">Configurazione password errata.</p>';
    }
    // Username validity and password validity have been checked client side.
    // This should should be adequate as nobody gains any advantage from
    // breaking these rules.
    //

    if (empty($error_msg)) {
            $newPassword = password_hash($newPassword, PASSWORD_BCRYPT);
            $stmt = $mysqli->prepare("UPDATE members SET password=?, email=?, cellulare=? WHERE username=?");
            $stmt->bind_param('ssss', $newPassword, $email, $phone, $username,);
            if (!$stmt->execute()) {
                echo "Inserimento OK";
            }else {
                echo "No inserime";
            }
    }
    // if (empty($error_msg)) {
    //     $prep_stmt = "SELECT id FROM members WHERE username = ? LIMIT 1";
    //     $stmt = $mysqli->prepare($prep_stmt);
    //
    //     if ($stmt) {
    //         $stmt->bind_param('s', $username);
    //         $stmt->execute();
    //         $stmt->store_result();
    //         $stmt->bind_result($user_id);
    //         var_dump($user_id);
    //         var_dump($username);
    //         if ($stmt->num_rows == 1) {
    //             $newPassword = password_hash($newPassword, PASSWORD_BCRYPT);
    //             $stmt = $mysqli->prepare("UPDATE members SET username=?, password=?, email=?, cellulare=? WHERE id=?");
    //             $stmt->bind_param('ssssi', $username, $newPassword, $email, $phone, $user_id);
    //             if (!$insert_stmt->execute()) {
    //                 header('Location: ../error.php?err=Errore di modifica: UPDATE');
    //             }
    //         }else {
    //             $error_msg .= '<p class="error">User non trovato.</p>';
    //         }
    //         echo "fine fine php";
    //     }
    // }
}
