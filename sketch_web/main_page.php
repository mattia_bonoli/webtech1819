<!DOCTYPE html>
<html lang="en">
<head>
    <title>Main Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" type="text/css" href="style/main_page_style.css">
    <link rel="stylesheet" type="text/css" href="style/modal.css">
    <link rel="stylesheet" type="text/css" href="style/footer.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>

<script>
    function adapt() {
        if ($(window).width() <= 360){
            $(".nav-btn").removeClass("fa-2x")
            $(".nav-btn").addClass("fa-1x")
        } else {
            $(".nav-btn").removeClass("fa-1x")
            $(".nav-btn").addClass("fa-2x")
        }
        if ($(window).width() <= 768){
            $("#search-2").hide();
            $(".search").show();
        } else {
            $("#search-2").show();
            $(".search").hide();

        }
    }

    $(document).ready(function f() {
        adapt();
        $("#search-1").hide();

        var menuHidden = true;

        $("#search").click(function f() {
            if(menuHidden) {
                $("#search-1").slideDown();
            } else {
                $("#search-1").slideUp();
            }
            menuHidden = !menuHidden;
        });

        $(".search-bar").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".res").filter(function() {
                $(this).toggle($(this).children().children(".res-name").text().toLowerCase().indexOf(value) > -1);
            })
        });

        $(".nav-link").click(function() {
            var resType = $(this).text();
            console.log(resType);
            $(".res").filter(function() {
                $(this).toggle($(this).attr('name') === resType);
            })
            $("#search-1").slideUp();
            menuHidden = !menuHidden;
        })

        $(".dropdown-item").click(function() {
            var resType = $(this).text();
            console.log(resType);
            $(".res").filter(function() {
                $(this).toggle($(this).attr('name') === resType);
            })
            $("#search-1").slideUp();
            menuHidden = !menuHidden;
        })

        $(".tutti").click(function () {
            $(".res").toggle(true);
            $("#search-1").slideUp();
            menuHidden = !menuHidden;
        })

        $( window ).resize(function() {
            adapt();
        })

    })
</script>

<?php
    include_once 'includes/db_connect.php';
    include_once 'includes/functions.php';
    sec_session_start();

    $logged = login_check($mysqli);

    if($logged && $_SESSION["userType"] == "business") {
        header('Location: notifiche.php');
    }

    $query = "SELECT *
              FROM ristoranti";

    $res = $mysqli->query($query);
?>

<body>
    <header class="py-2 bg-dark">
        <div class="container-fluid">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4">
                    <a id="slide" class="text-muted" href="#">
                        <a id="slide" class="text-muted" href="#">
                            <i class="fas fas fa-angle-right fa-2x" data-toggle="modal" data-target="#sideModal"></i>
                        </a>
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="#"><img id="logo" src="res/logo.png" alt="" width="30" height="30"></a>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                    <a id="search" class="text-muted nav-btn" href="#">
                        <i class="fas fa-search search"></i>
                        <?php
                            if($logged){
                                ?>
                                <a href="includes/logout.php"><button class="btn btn-danger account" type="submit">Esci</button></a>
                                <?php
                            } else {
                                ?>
                                <a href="login.php"><button class="btn btn-success login account" type="submit" style="margin-right: 20px;">Login</button></a>
                                <a href="register.php"><button class="btn btn-secondary account" type="submit">Sign in</button></a>
                                <?php
                            }
                        ?>
                    </a>
                </div>
            </div>
        </div>
    </header>

    <div class="modal left fade" id="sideModal" tabindex="-1" role="dialog" aria-labelledby="sideModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="list-group list-group-flush">
                        <?php
                        if (login_check($mysqli) == true) {
                            echo '<h4 class="py-3">Benvenuto, '. htmlentities($_SESSION['username']) . '</h4>';
                            echo '<a href="notifiche.php" class="btn btn-light btn-lg btn-block m-1">Notifiche</a>';
                            echo '<a href="user.php" class="btn btn-light btn-lg btn-block m-1">Gestisci Account</a>';
                            if($_SESSION["userType"] === "user") {
                                echo '<a href="orders.php" class="btn btn-light btn-lg btn-block m-1">I miei ordini</a>';
                            }
                            echo '<a href="includes/logout.php" class="btn btn-light btn-lg btn-block m-1">Log out</a>';
                        }else {
                            echo '<h4 class="pb-3">Per accedere a queste pagine, effettua il <a href="login.php">login</a>.</h4>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Notifiche</a>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Gestisci Account</a>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">I miei ordini</a>';

                        }
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container" id="main">

        <?php if (isset($_SESSION['show_notif_ok'])) {
            echo '<div class="alert alert-success alert-dismissible fade show">
                      <button type="button" class="close" data-dismiss="alert">&times;</button>
                      <strong>Ordine completato con successo!</strong>
                </div>';
            unset($_SESSION['show_notif_ok']);
        } ?>

        <div id="search-1">
            <div class="input-group mb-3">
                <input id="search-bar" type="text" class="form-control search-bar" placeholder="Cerca per nome...">
            </div>
            <nav class="navbar navbar-expand-lg justify-content-center rounded border">
                <ul class="navbar-nav">
                    <li class="nav-item">
                        <a class="nav-link" href="#">Pizzeria</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Piadineria</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Fast food</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Romagnolo</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Vegano</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Etnico</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Italiano</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Gourmet</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Altro</a>
                    </li>
                    <li class="nav-item tutti">
                        <a class="nav-link" href="#">Tutti</a>
                    </li>
                </ul>
            </nav>
        </div>

        <div id="search-2" class="input-group mt-3 mb-3">
            <div class="input-group-prepend">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    Categorie
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Pizzeria</a>
                    <a class="dropdown-item" href="#">Piadineria</a>
                    <a class="dropdown-item" href="#">Fast food</a>
                    <a class="dropdown-item" href="#">Romagnolo</a>
                    <a class="dropdown-item" href="#">Vegano</a>
                    <a class="dropdown-item" href="#">Etnico</a>
                    <a class="dropdown-item" href="#">Italiano</a>
                    <a class="dropdown-item" href="#">Gourmet</a>
                    <a class="dropdown-item" href="#">Altro</a>
                    <a class="dropdown-item tutti" href="#">Tutti</a>
                </div>
            </div>
            <input id="search-input" type="text" class="form-control search-bar" placeholder="Cerca per nome...">
        </div>

        <div class="flex-container">
        <?php
            while($row = mysqli_fetch_assoc($res)) {
            ?>
                <div name="<?php echo $row["categoria"]; ?>" class="rounded bg-light res">
                    <a href="restaurant_page.php?codice_ristorante=<?php echo $row["codice_ristorante"]; ?>">
                    <div>
                        <img class="rounded-top" src="<?php echo $row["path_immagine_copertina"]; ?>" alt="">
                    </div>
                    <div class="title font-weight-bold res-name">
                        <?php
                            echo $row["nome"];
                        ?>
                    </div>
                    </a>
                </div>
            <?php
            }
            ?>
        </div>
    </div>
    <footer>
        <div class="footer-copyright text-center py-2 bg-dark" style="color: white;">
            Tecnologie web 2018/2019 <br>
            Credits: Filippo Pistocchi, Mattia Bonoli, Federico Cichetti.
        </div>
    </footer>
</body>
</html>
