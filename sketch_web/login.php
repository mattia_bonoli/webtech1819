<?php
include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" type="text/css" href="style/modal.css">
    <link rel="stylesheet" type="text/css" href="style/footer.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script type="text/JavaScript" src="js/sha512.js"></script>
    <script type="text/JavaScript" src="js/forms.js"></script>
</head>
<body>
    <header class="py-2 bg-dark">
        <div class="container-fluid">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4">
                    <a id="slide" class="text-muted" href="#">
                        <i class="fas fas fa-angle-right fa-2x" data-toggle="modal" data-target="#sideModal"></i>
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="main_page.php"><img id="logo" src="res/logo.png" alt="Logo" width="45" height="45"></a>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                </div>
            </div>
        </div>
    </header>
<div class="modal left fade" id="sideModal" tabindex="-1" role="dialog" aria-labelledby="sideModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-body text-center">
                <div class="list-group list-group-flush">
                    <?php
                    if (login_check($mysqli) == true) {
                        echo '<h4 class="py-3">Benvenuto, '. htmlentities($_SESSION['username']) . '</h4>';
                        echo '<a href="notifiche.php" class="btn btn-light btn-lg btn-block m-1">Notifiche</a>';
                        echo '<a href="user.php" class="btn btn-light btn-lg btn-block m-1">Gestisci Account</a>';
                        if($_SESSION["userType"] == "business") {
                            echo '<a href="orders.php" class="btn btn-light btn-lg btn-block m-1">I miei ordini</a>';
                        }
                        echo '<a href="includes/logout.php" class="btn btn-light btn-lg btn-block m-1">Log out</a>';
                    }else {
                        echo '<h4 class="pb-3">Per accedere a queste pagine, effettua il <a href="login.php">login</a>.</h4>';
                        echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Notifiche</a>';
                        echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Gestisci Account</a>';
                        echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">I miei ordini</a>';

                    }
                    ?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="container py-3">
    <?php
    if (isset($_GET['error'])) {
        echo '<p class="error">Errore di Login!</p>';
    }
    ?>
    <form action="includes/process_login.php" method="post" name="login_form">
        <div class="form-group">
            <label for="email">Email:</label>
            <input type="email" class="form-control" id="email" name="email">
        </div>
        <div class="form-group">
            <label for="pwd">Password:</label>
            <input type="password" class="form-control" id="pwd" name="password">
        </div>
        <button type="submit" class="btn btn-primary" onclick="formhash(this.form, this.form.password);">Log In</button>
    </form>
    <?php
    if (login_check($mysqli) == true) {
        echo '<p class="mt-3">Attualmente hai effettuato l\'accesso come ' . htmlentities($_SESSION['username']) . ', utente di tipologia: ' . $_SESSION['userType'] . '.' .'</p>';

        echo '<p>Non sei tu? <a href="includes/logout.php">Log out</a>.</p>';
    } else {
        echo '<p>Attualmente non hai effettuato l\'accesso.</p>';
        echo "<p>Non hai un account? <a href='register.php'>Registrati.</a></p>";
        echo "<p>Test user is asd@asd.it:asdASD123</p>";
    }
    ?>
</div>
<!-- [TODO] Password rocovery. Remove login status -->
<footer>
    <div class="footer-copyright text-center py-3 bg-dark fixed-bottom" style="color: white;">
        Tecnologie Web 2018/2019 <br>
        Credits: Filippo Pistocchi, Mattia Bonoli, Federico Cichetti.
    </div>
</footer>
</body>
</html>
