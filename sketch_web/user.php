<?php
include_once 'includes/functions.php';
include_once 'includes/db_connect.php';
include_once 'includes/edit.inc.php';
sec_session_start();
if (login_check($mysqli) == false) {
            header('Location: login.php');
}
$username = htmlentities($_SESSION['username'])
?>
<!-- echo $query="UPDATE members
SET username = '$username', fname = '$fname', email = '$email', password = '$password'
WHERE id=$id"; -->
<!DOCTYPE html>
<head>
    <title>Pagina Utente</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" type="text/css" href="style/user.css">
    <link rel="stylesheet" type="text/css" href="style/modal.css">
    <link rel="stylesheet" type="text/css" href="style/footer.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script type="text/JavaScript" src="js/sha512.js"></script>
    <script type="text/JavaScript" src="js/forms.js"></script>
</head>
<body>
    <header class="py-3 bg-dark">
        <div class="container-fluid">
            <div class="row flex-nowrap align-items-center">
                <div class="col-4">
                    <a id="slide" class="text-muted" href="#">
                        <i class="fas fas fa-angle-right fa-2x" data-toggle="modal" data-target="#sideModal"></i>
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="landing.php"><img src="logo.png" alt="" width="50" height="50"></a>
                </div>
            </div>
        </div>
    </header>
    <div class="modal left fade" id="sideModal" tabindex="-1" role="dialog" aria-labelledby="sideModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="list-group list-group-flush">
                        <?php
                        if (login_check($mysqli) == true) {
                            echo '<h4 class="py-3">Benvenuto, '. $username . '</h4>';
                            echo '<a href="notifiche.php" class="btn btn-light btn-lg btn-block m-1">Notifiche</a>';
                            if($_SESSION["userType"] == "business") {
                                echo '<a href="images.php" class="btn btn-light btn-lg btn-block m-1">Gestisci Immagini</a>';
                                echo '<a href="modify_menu.php" class="btn btn-light btn-lg btn-block m-1">Modifica Menù</a>';
                            }
                            if($_SESSION["userType"] === "user") {
                                echo '<a href="orders.php" class="btn btn-light btn-lg btn-block m-1">I miei ordini</a>';
                            }
                            echo '<a href="includes/logout.php" class="btn btn-light btn-lg btn-block m-1">Log out</a>';
                        }else {
                            echo '<h4 class="pb-3">Per accedere a queste pagine, effettua il <a href="login.php">login</a>.</h4>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Notifiche</a>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Gestisci Account</a>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">I miei ordini</a>';

                        }
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <div class="container py-3">
        <h2>In questa pagina è possibile modificare i propri dati inseriti.</h2>
        <form action="<?php echo esc_url($_SERVER['REQUEST_URI']); ?>" method="post" name="edit_form">
            <div class="form-group">
                <label for="usr">Username:</label>
                <input type="text" class="form-control" value="<?php echo $username;?>" id="usr" name="username" readonly>
            </div>
            <!-- Dati Modificabili -->
            <div class="form-group">
                <label for="phone">Cellulare:</label>
                <input type="number" class="form-control" id="phone" name="phone">
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <input type="email" class="form-control" id="email" name="email">
            </div>
            <div class="form-group">
                <label for="pwd">Vecchia Password:</label>
                <input type="password" class="form-control" id="oldpwd" name="oldpassword">
            </div>
            <div class="form-group">
                <label for="pwd">Nuova Password:</label>
                <input type="password" class="form-control" id="pwd" name="password">
            </div>
            <div class="form-group">
                <label for="confirmpwd">Conferma password:</label>
                <input type="password" class="form-control" id="confirmpwd" name="confirmpwd">
            </div>
            <button type="submit" class="btn btn-primary" onclick="return editformhash(this.form,
                            this.form.email,
                            this.form.oldpassword,
                            this.form.password,
                            this.form.confirmpwd);">Aggiorna Dati
            </button>
    </div>
    <footer>
        <div class="footer-copyright text-center py-3 bg-dark fixed-bottom" style="color: white;">
            Tecnologie Web 2018/2019 <br>
            Credits: Filippo Pistocchi, Mattia Bonoli, Federico Cichetti.
        </div>
    </footer>
</body>
