<!DOCTYPE html>
<html lang="en">
<head>
    <title>Restaurant Page</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" type="text/css" href="style/restaurant_page_style.css">
    <link rel="stylesheet" type="text/css" href="style/modal.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>

<?php
    include_once 'includes/db_connect.php';
    include_once 'includes/functions.php';
    sec_session_start();

    $codice_ristorante = $_GET["codice_ristorante"];

    $query_restaurant = "select nome, descrizione, indirizzo
                         from ristoranti
                         where codice_ristorante = " . $codice_ristorante;

    $query_piatti = "select *
                     from piatti
                     where codice_ristorante = " . $codice_ristorante;

    $query_categorie = "select categoria
                        from piatti
                        where codice_ristorante = " . $codice_ristorante
                        . " group by categoria";

    $query_immagini = "select path_immagine
                       from immagini_copertina
                       where codice_ristorante = " . $codice_ristorante;


    $res_ristoranti = $mysqli->query($query_restaurant);
    $res_piatti = $mysqli->query($query_piatti);
    $res_categorie = $mysqli->query($query_categorie);
    $res_immagini = $mysqli->query($query_immagini);
    $num_immagini = $res_immagini->num_rows;

    $row_ristoranti = mysqli_fetch_assoc($res_ristoranti);

?>

<script>

    function adapt() {
        if ($(window).width() <= 360){
            $(".nav-btn").removeClass("fa-2x")
            $(".nav-btn").addClass("fa-1x")
        } else {
            $(".nav-btn").removeClass("fa-1x")
            $(".nav-btn").addClass("fa-2x")
        }
    }

    /*setting up order variable*/
    var order = {};

    $(document).ready(function(){
        adapt();
        $( window ).resize(function() {
            adapt();
        })
        /* initializing popovers */
        $('[data-toggle="popover"]').popover();

        var cont = 0;
        var cartItems = 0;
        $(".minus").click(function f() {
            var contString = $(this).parent().children(".price").text();
            var contInt = parseInt(contString);
            if(contInt !== 0){
                contInt--;
                $(this).parent().children(".price").html(contInt.toString());
                cartItems--;
                $(".cart-item").html(cartItems);
                //removing food from the order variable
                var foodName = $(this).parent().parent().children("#food-name").text();
                if (contInt === 0) {
                    delete order[foodName.toString()];
                } else {
                    order[foodName.toString()] = order[foodName.toString()] - 1;
                }
                if (cartItems === 0) {
                    $("#cart").removeClass("active");
                    $("#cart").addClass("disabled");
                }
            }
        });

        $(".plus").click(function f() {
            var contString = $(this).parent().children(".price").text();
            var contInt = parseInt(contString);
            if (cartItems === 0) {
                $("#cart").removeClass("disabled");
                $("#cart").addClass("active");
            }
            cartItems++;
            $(".cart-item").html(cartItems);
            contInt++;
            $(this).parent().children(".price").html(contInt.toString());

            //putting food into the order variable
            var foodName = $(this).parent().parent().children("#food-name").text();
            if (order.hasOwnProperty(foodName.toString())) {
                order[foodName.toString()] = order[foodName.toString()] + 1;
            } else {
                order[foodName.toString()] = 1;
            }
        });

        $("#search-input").on("keyup", function() {
            var value = $(this).val().toLowerCase();
            $(".food").filter(function() {
                $(this).toggle($(this).children(".name").text().toLowerCase().indexOf(value) > -1);
            })
        });

        $(".dropdown-item").click(function () {
            var resType = $(this).text();
            $(".food").filter(function() {
                $(this).toggle($(this).attr('name') === resType);
            })
        })

        $(".Tutti").click(function () {
            $(".food").toggle(true);
        })
    });

    function sendJSON() {
        //adding codice_ristorante in JSON object for callback
        var codice_ristorante = <?php echo $codice_ristorante ?>;
        order["codice_ristorante"] = codice_ristorante;
        //send json object via get (consider trying POST instead)
        JSONorder = JSON.stringify(order);
        window.location.href = "cart.php?order=" + JSONorder;
    }

</script>

<body>
    <header class="py-2 bg-dark">
        <div class="container-fluid">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4">
                    <a id="slide" class="text-muted" href="#">
                        <i class="fas fas fa-angle-right fa-2x nav-btn"></i>
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="main_page.php"><img id="logo" src="res/logo.png" alt="" width="30" height="30"></a>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                    <button class="btn btn-link disabled" onclick="sendJSON()" id="cart">
                        <i class="fas fa-cart-plus fa-2x nav-btn"></i>
                    </button>
                    <span class="badge badge-danger cart-item">0</span>
                </div>
            </div>
        </div>
    </header>

    <div class="modal left fade" id="sideModal" tabindex="-1" role="dialog" aria-labelledby="sideModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="list-group list-group-flush">
                        <?php
                        if (login_check($mysqli) == true) {
                            echo '<h4 class="py-3">Benvenuto, '. htmlentities($_SESSION['username']) . '</h4>';
                            echo '<a href="notifiche.php" class="btn btn-light btn-lg btn-block m-1">Notifiche</a>';
                            echo '<a href="user.php" class="btn btn-light btn-lg btn-block m-1">Gestisci Account</a>';
                            if($_SESSION["userType"] == "business") {
                                echo '<a href="orders.php" class="btn btn-light btn-lg btn-block m-1">I miei ordini</a>';
                            }
                            echo '<a href="includes/logout.php" class="btn btn-light btn-lg btn-block m-1">Log out</a>';
                        }else {
                            echo '<h4 class="pb-3">Per accedere a queste pagine, effettua il <a href="login.php">login</a>.</h4>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Notifiche</a>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Gestisci Account</a>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">I miei ordini</a>';

                        }
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <div class="container" id="main">


        <div id="demo" class="carousel slide" data-ride="carousel">

            <!-- Indicators -->
            <ul class="carousel-indicators">
            <?php
                for ($i=0; $i < $num_immagini; $i++) {
                    if ($i === 0) {
                        ?><?php
                        ?><li data-target="#demo" data-slide-to="<?php echo $i?>" class="active"></li><?php
                    } else {
                        ?><li data-target="#demo" data-slide-to="<?php echo $i?>"></li><?php
                    }
                }
            ?>
            </ul>

            <!-- The slideshow -->
            <div class="carousel-inner">
                <?php
                    for ($i=0; $row_immagini = mysqli_fetch_assoc($res_immagini); $i++) {
                        if ($i === 0) {
                            ?>
                            <div class="carousel-item active">
                                <img id="carousel-image" src="<?php echo $row_immagini["path_immagine"] ?>" alt="" width="100%">
                            </div>
                            <?php
                        } else {
                            ?>
                            <div class="carousel-item">
                                <img id="carousel-image" src="<?php echo $row_immagini["path_immagine"] ?>" alt="" width="100%">
                            </div>
                            <?php
                        }
                    }
                ?>
            </div>

            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#demo" data-slide="prev">
            <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#demo" data-slide="next">
            <span class="carousel-control-next-icon"></span>
            </a>
        </div>

        <div id="res-name" class="bg-light">
            <h1><?php echo $row_ristoranti["nome"]; ?></h1>
            <ul>
                <li><p class="res-p"><?php echo $row_ristoranti["indirizzo"]; ?></p></li>
                <li><p class="res-p"><?php echo $row_ristoranti["descrizione"]; ?></p></li>
            </ul>
        </div>

        <div id="search-bar" class="input-group mt-3 mb-3">
            <div class="input-group-prepend">
                <button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
                    Categorie
                </button>
                <div class="dropdown-menu">
                    <?php
                        while($row_categorie = mysqli_fetch_assoc($res_categorie)){
                    ?>
                        <a class="dropdown-item" href="#"><?php echo $row_categorie["categoria"]?></a>
                    <?php
                        }
                    ?>
                        <a class="dropdown-item Tutti" href="#">Tutti</a>
                </div>
            </div>
            <input id="search-input" type="text" class="form-control" placeholder="Cerca...">
        </div>

        <div id="food-container" class="flex-container">
            <?php
                while ($row_piatti = mysqli_fetch_assoc($res_piatti)){
            ?>
            <div name="<?php echo $row_piatti["categoria"]?>" class="rounded bg-light food">
                <div id="food-name" class="title font-weight-bold name"><?php echo $row_piatti["nome"] ?></div>
                <ul>
                    <li><p class="food-p"><?php echo $row_piatti["descrizione"]; ?></p></li>
                    <li><p class="food-p">€<?php echo $row_piatti["prezzo"];?></p></li>
                </ul>
                <div id="food-info">
                    <button type="button" class="btn minus">-</button>
                    <p class="price">0</p><p class="x">x</p>
                    <button type="button" class="btn plus">+</button>
                    <button id="info" data-toggle="popover" data-placement="top" data-content="Allergeni: <?php echo $row_piatti["allergeni"]?>" type="button" class="btn btn-info">
                        <i class="fas fa-info-circle info"></i>
                    </button>
                </div>
            </div>
            <?php
                }
            ?>
        </div>
    </div>
    <footer>
        <div class="footer-copyright text-center py-2 bg-dark" style="color: white;">
            Tecnologie web 2018/2019 <br>
            Credits: Filippo Pistocchi, Mattia Bonoli, Federico Cichetti.
        </div>
    </footer>
</body>
</html>
