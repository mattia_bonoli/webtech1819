<!DOCTYPE html>
<html lang="it">
    <head>
        <meta charset="utf-8">
        <title>Notification data</title>
    </head>
    <body>
        <div id="notifiche">
            <?php
            include_once 'includes/notification_functions.php';
            sec_session_start();
            if (isset($_GET["type"])) {
                $account_type = $_GET["type"];
                switch ($account_type) {
                    case 'business':
                        $qry = "SELECT * FROM notifica_per_ristorante WHERE codice_ristorante=(SELECT codice_ristorante FROM ristoranti WHERE id=" . $_SESSION['user_id'] . ")";
                        break;
                    case 'user':
                        $qry = "SELECT * FROM notifica_per_utente WHERE id=" . $_SESSION['user_id'];
                        break;
                    default:
                        echo "Errore di connessione al database!";
                        break;
                }
                $ris = $mysqli->query($qry);
                if($ris->num_rows>0) {
                    while ($row = $ris->fetch_assoc()) {
            ?>
            <div class="jumbotron notifica">
                <?php echo $row["descrizione"]; ?>
                <hr>
                <?php
                    $qry_orario = "SELECT ora_prenotazione, luogo_consegna, id FROM prenotazioni WHERE codice_prenotazione=" . $row["codice_prenotazione"];
                    $ora = $mysqli->query($qry_orario);
                    $orarow = $ora->fetch_assoc();
                 ?>
                 <h1>Orario richiesto: <?php echo $orarow["ora_prenotazione"]; ?></h1>
                 <!-- ingr1 = via abba 3, ingr 2 = via babba 4 -->
                 <h2>Presso: <?php echo $orarow["luogo_consegna"] === 'ingr1' ? 'Via Abba 3' : 'Via Babba 4'; ?> </h2>
                 <label name="hours" class="hours">Posticipa a:
                 <select class="custom-select">
                 <?php
                 $hours = fill_hours();
                 foreach ($hours as $h) {
                 ?>
                    <option><?php echo $h; ?></option>
                 <?php
                    }
                 ?>
                 </select>
                 </label>
                 <button type="button" class="btn btn-danger delete" name="delete">Elimina</button>
                 <button type="button" class="btn btn-primary conferma" name="confirm">Conferma</button>
                 <input type="hidden" name="seen" value="<?php echo $row["letta"]; ?>">
                 <input type="hidden" name="cod" value="<?php echo $row["codice_prenotazione"]; ?>">
                 <input type="hidden" name="cod_rist" value="<?php echo $row["codice_notifica_ristorante"]; ?>">
                 <input type="hidden" name="id_ut" value="<?php echo $orarow["id"]; ?>">
            </div>
            <?php
                    }
                }
            }
             ?>
        </div>
    </body>
</html>
