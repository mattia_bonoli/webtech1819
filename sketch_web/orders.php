<!DOCTYPE html>
<html lang="it">
<head>
    <title>Ordini</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">
    <link rel="stylesheet" href="style/main_page_style.css">
    <link rel="stylesheet" href="style/notifiche.css">
    <link rel="stylesheet" href="style/orders.css">
    <link rel="stylesheet" type="text/css" href="style/modal.css">
    <link rel="stylesheet" href="style/footer.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
</head>
<body>
    <header class="py-2 bg-dark">
        <div class="container-fluid">
            <div class="row flex-nowrap justify-content-between align-items-center">
                <div class="col-4">
                    <a id="slide" class="text-muted" href="#">
                        <i class="fas fas fa-angle-right fa-2x" data-toggle="modal" data-target="#sideModal"></i>
                    </a>
                </div>
                <div class="col-4 text-center">
                    <a href="main_page.php"><img id="logo" src="res/logo.png" alt="Logo" width="30" height="30"></a>
                </div>
                <div class="col-4 d-flex justify-content-end align-items-center">
                </div>
            </div>
        </div>
    </header>
    <div class="modal left fade" id="sideModal" tabindex="-1" role="dialog" aria-labelledby="sideModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="list-group list-group-flush">
                        <?php
                        include_once 'includes/db_connect.php';
                        include_once 'includes/functions.php';
                        sec_session_start();
                        $logged = login_check($mysqli);
                        if (login_check($mysqli) == true) {
                            echo '<h4 class="py-3">Benvenuto, '. htmlentities($_SESSION['username']) . '</h4>';
                            echo '<a href="notifiche.php" class="btn btn-light btn-lg btn-block m-1">Notifiche</a>';
                            echo '<a href="user.php" class="btn btn-light btn-lg btn-block m-1">Gestisci Account</a>';
                            echo '<a href="includes/logout.php" class="btn btn-light btn-lg btn-block m-1">Log out</a>';
                        }else {
                            echo '<h4 class="pb-3">Per accedere a queste pagine, effettua il <a href="login.php">login</a>.</h4>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Notifiche</a>';
                            echo '<a href="#" class="btn btn-light btn-lg btn-block m-1 disabled">Gestisci Account</a>';

                        }
                        ?>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <?php
        if($logged) {
            if ($_SESSION["userType"] === "user") { //only users can see their orders
    ?>
    <p id="no-not">Non c'è nulla da mostrare...</p>
    <section>
        <div class="container">
            <?php
                $qry = "SELECT * FROM prenotazioni WHERE id=" . $_SESSION["user_id"] . " ORDER BY codice_prenotazione DESC";
                if($ris = $mysqli->query($qry)){
                    while($order_row = $ris->fetch_assoc()) {
             ?>
            <div class="jumbotron ordine">
                <h1>Ordine effettuato il giorno <?php $date = date_create($order_row["data_prenotazione"]); echo date_format($date,"d/m/Y");?>
                per le ore <?php echo $order_row["ora_prenotazione"];?></h1>
                <section>
                    <a data-toggle="collapse" data-target="#details<?php echo $order_row['codice_prenotazione'];?>" class="detail-trigger">Dettagli</a>
                    <div id=details<?php echo $order_row['codice_prenotazione'];?> class="collapse">
                        <ul class="list1">
                            <li>Luogo consegna: <?php echo $order_row["luogo_consegna"] === 'ingr1' ? 'Via Abba 3' : 'Via Babba 4'; ?></li>
                            <li>Riassunto ordine:
                                <ul>
                                    <?php
                                        $qry_ptt = "SELECT numero_piatti, piatti.nome FROM piatti_in_prenotazione
                                        INNER JOIN piatti ON piatti.codice_piatto=piatti_in_prenotazione.codice_piatto
                                        WHERE codice_prenotazione=" . $order_row['codice_prenotazione'];
                                        if($risptt = $mysqli->query($qry_ptt)){
                                            while($piatti_row = $risptt->fetch_assoc()) {
                                    ?>
                                    <li><?php echo $piatti_row["numero_piatti"]; ?>x <?php echo $piatti_row["nome"]; ?></li>
                                <?php } ?>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
            <?php
                        }
                    }
                }
             ?>
        </div>
    </section>
    <?php
        } else {
            header('Location: main_page.php');
        }
    $mysqli->close();
    } else {
        header('Location: login.php');
    }
    ?>
    <footer>
        <div class="footer-copyright text-center py-2 bg-dark" style="color: white;">
            Tecnologie web 2018/2019 <br>
            Credits: Filippo Pistocchi, Mattia Bonoli, Federico Cichetti.
        </div>
    </footer>
</body>
<script type="text/javascript">
    $(document).ready(function(){
        var elementsNum = $(".ordine").length;
        if (elementsNum > 0) {
            $("#no-not").hide();
        }
        $("#reorder").click(function(){

        })
    })
</script>
</html>
