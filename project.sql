-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Feb 18, 2019 alle 14:52
-- Versione del server: 10.1.37-MariaDB
-- Versione PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `project`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_copertina`
--

CREATE TABLE `immagini_copertina` (
  `path_immagine` varchar(500) NOT NULL,
  `codice_ristorante` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `immagini_copertina`
--

INSERT INTO `immagini_copertina` (`path_immagine`, `codice_ristorante`) VALUES
('res/mascia2.jpg', 1),
('res/mascia3.jpg', 1),
('res/mascia4.jpg', 1),
('res/napizz.jpg', 2),
('res/napizz1.jpg', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `name`  varchar(30),
  `surname`  varchar(30),
  `partita_iva` char(11),
  `ragione_sociale` varchar(100),
  `cellulare` varchar(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` char(128) NOT NULL,
  `type` varchar(30) NOT NULL COMMENT 'Type of user account'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `type`) VALUES
(1, 'asd', 'asd@asd.it', '$2y$10$wuHelqC3QH.8vZs.iTKQfuRZY1uXI6AhHtyKW6fWUU4/5Ru4RYddu', 'business');


-- --------------------------------------------------------

--
-- Struttura della tabella `notifica_per_ristorante`
--

CREATE TABLE `notifica_per_ristorante` (
  `codice_notifica_ristorante` bigint(20) NOT NULL,
  `codice_prenotazione` bigint(20) NOT NULL,
  `descrizione` varchar(1000) NOT NULL,
  `letta` char(1) NOT NULL,
  `codice_ristorante` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `notifica_per_utente`
--

CREATE TABLE `notifica_per_utente` (
  `codice_notifica_utente` bigint(20) NOT NULL,
  `codice_prenotazione` bigint(20) NOT NULL,
  `codice_notifica_ristorante` bigint(20) NOT NULL,
  `descrizione` varchar(1000) NOT NULL,
  `letta` char(1) NOT NULL,
  `id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `piatti`
--

CREATE TABLE `piatti` (
  `codice_piatto` bigint(20) NOT NULL,
  `nome` varchar(50) NOT NULL,
  `categoria` varchar(50) NOT NULL,
  `prezzo` varchar(10) NOT NULL,
  `descrizione` varchar(500) NOT NULL,
  `allergeni` varchar(500) NOT NULL,
  `codice_ristorante` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `piatti`
--

INSERT INTO `piatti` (`codice_piatto`, `nome`, `categoria`, `prezzo`, `descrizione`, `allergeni`, `codice_ristorante`) VALUES
(1, 'CSR', 'Piadine', '3.50', 'Piadina crudo sququerone e rucola', 'Latticini e glutine', 1),
(2, 'margherita', 'Pizza', '5.00', 'Pizza pomodoro e mozzarella', 'Glutine e latticini', 2),
(3, 'Napoli', 'Pizza', '5.00', 'Pizza pomodoro, mozzarella e acciughe', 'Glutine e latticini', 2),
(4, 'Bufala', 'Pizza', '5.00', 'Pizza pomodoro e mozzarella di bufala', 'Glutine e latticini', 2),
(5, 'Napizz', 'Pizza con bordi ripieni', '7.00', 'Pizza pomodoro, mozzarella e acciughe con bordi ripieni di ricotta', 'Glutine e latticini', 2),
(6, 'Marinara', 'Pizza', '4.00', 'Pizza pomodoro origano e aglio', 'Glutine', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `piatti_in_prenotazione`
--

CREATE TABLE `piatti_in_prenotazione` (
  `numero_piatti` int(11) NOT NULL,
  `codice_piatto` bigint(20) NOT NULL,
  `codice_prenotazione` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `prenotazioni`
--

CREATE TABLE `prenotazioni` (
  `codice_prenotazione` int(11) NOT NULL,
  `data_prenotazione` date NOT NULL,
  `ora_prenotazione` varchar(10) NOT NULL,
  `luogo_consegna` varchar(20) NOT NULL,
  `id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Struttura della tabella `ristoranti`
--

CREATE TABLE `ristoranti` (
  `codice_ristorante` bigint(20) NOT NULL,
  `nome` varchar(100) NOT NULL,
  `categoria` varchar(50) NOT NULL,
  `indirizzo` varchar(500) NOT NULL,
  `descrizione` varchar(500) NOT NULL,
  `path_immagine_copertina` varchar(1000) NOT NULL,
  `id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ristoranti`
--

INSERT INTO `ristoranti` (`codice_ristorante`, `nome`, `categoria`, `indirizzo`, `descrizione`, `path_immagine_copertina`, `id`) VALUES
(1, 'La Piadina di Stefano e Mascia', 'Piadineria', 'Viale Gaspare Finali, 5, 47521, Cesena FC', 'Piadineria in centro a Cesena vicino alla barriera', 'res/mascia.jpg', 1),
(2, 'Napizz', 'Pizzeria', 'Via Natale Dell\'Amore 7, 47521, Cesena', 'Pizzeria vicino alla barriera', 'res/napizz.jpg', 9);

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `immagini_copertina`
--
ALTER TABLE `immagini_copertina`
  ADD PRIMARY KEY (`path_immagine`);

--
-- Indici per le tabelle `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indici per le tabelle `notifica_per_ristorante`
--
ALTER TABLE `notifica_per_ristorante`
  ADD PRIMARY KEY (`codice_notifica_ristorante`),
  ADD UNIQUE KEY `FKR_2_ID` (`codice_prenotazione`);

--
-- Indici per le tabelle `notifica_per_utente`
--
ALTER TABLE `notifica_per_utente`
  ADD PRIMARY KEY (`codice_notifica_utente`),
  ADD UNIQUE KEY `FKR_2_ID` (`codice_prenotazione`);

--
-- Indici per le tabelle `piatti`
--
ALTER TABLE `piatti`
  ADD PRIMARY KEY (`codice_piatto`);

--
-- Indici per le tabelle `piatti_in_prenotazione`
--
ALTER TABLE `piatti_in_prenotazione`
  ADD PRIMARY KEY (`codice_piatto`,`codice_prenotazione`);

--
-- Indici per le tabelle `prenotazioni`
--
ALTER TABLE `prenotazioni`
  ADD PRIMARY KEY (`codice_prenotazione`);

--
-- Indici per le tabelle `ristoranti`
--
ALTER TABLE `ristoranti`
  ADD PRIMARY KEY (`codice_ristorante`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT per la tabella `notifica_per_ristorante`
--
ALTER TABLE `notifica_per_ristorante`
  MODIFY `codice_notifica_ristorante` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `notifica_per_utente`
--
ALTER TABLE `notifica_per_utente`
  MODIFY `codice_notifica_utente` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `piatti`
--
ALTER TABLE `piatti`
  MODIFY `codice_piatto` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `prenotazioni`
--
ALTER TABLE `prenotazioni`
  MODIFY `codice_prenotazione` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT per la tabella `ristoranti`
--
ALTER TABLE `ristoranti`
  MODIFY `codice_ristorante` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
